# Bank Mandiri Test



## 1
a. Java Spring services yang di deploy di Kubernetes dengan platform Openshift. (platform ini memiliki service worker sebagai pod untuk running kubernetes environment services dan app)  

b. Jenkins memungkinkan untuk membuat deployment automation dari source code repository menjadi running app di beberapa environment (dev, uat, prod, etc)  

c. Kibana memungkinkan untuk melakukan beberapa log API request response dan app monitoring  

d. Beberapa service akan melakukan operasi pada shared database/shared core service seperti core banking system dan expose API menggunakan standar OpenAPI sebagai dokumentasi untuk di consume di public domain.

## 2
https://drive.google.com/file/d/1Tmqig5jvhSGze28qjLWsPeMOnnPfc8oi/view?usp=sharing
```
openapi: 3.0.3
info:
  title: PINJOL API
  description: Pinjaman Online Api Spec
  version: 1.0.11
servers:
  - url: /api
tags:
  - name: customer
    description: Customer Registration and Inquiry
  - name: loan
    description: Loan creation and inquiry
  - name: pay
    description: Loan payment and inquiry
paths:
  /customer:
    post:
      tags:
        - customer
      summary: Add new customer
      description: Add new customer
      operationId: addCustomer
      requestBody:
        description: Create new customer
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Post_Customer'
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Post_Customer'          
        '400':
          description: Bad Request
        '501':
          description: Data tidak sesuai dengan identitas
  /customer/{username}:
    get:
      tags:
        - customer
      summary: Get customer
      description: Get customer
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
          description: username
      responses:
        '200':
          description: Successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Get_Customer'          
        '404':
          description: Customer not found or Customer inactive
  /loan:
    post:
      tags:
        - loan
      summary: Add new loan
      description: Add new loan to the customer
      operationId: addLoan
      requestBody:
        description: Create new loan
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Post_Loan'
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Post_Loan'          
        '400':
          description: Bad Request
        '502':
          description: Mencapai limit pengajuan pinjaman
  /loan/{username}:
    get:
      tags:
        - loan
      summary: Get loan
      description: Get loan
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
          description: username
      responses:
        '200':
          description: Successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Get_Loan'          
        '404':
          description: Loan not found
  /pay/{loan_id}:
    post:
      tags:
        - pay
      summary: Pay loan
      description: Pay loan
      parameters:
        - in: path
          name: loan_id
          schema:
            type: string
          required: true
          description: Loan id
      responses:
        '200':
          description: Successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Post_Pay_Response'          
        '404':
          description: Loan id not found
  /pay/status/{trx_id}:
    get:
      tags:
        - pay
      summary: Get payment status
      description: Get payment status
      parameters:
        - in: path
          name: trx_id
          schema:
            type: string
          required: true
          description: transaction id
      responses:
        '200':
          description: Successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Get_Status_Response'          
        '404':
          description: Transaction id not found
components:
  schemas:
    Post_Customer:
      type: object
      properties:
        username:
          type: string
          example: jpaul
        nik:
          type: string
          example: 3374141414141414
        kk:
          type: string
          example: 3374010101010101
        fullname:
          type: string
          example: John Paul
        birthdate:
          type: string
          example: 01-01-1991
        mothersmaiden:
          type: string
          example: Paul Mother
    Get_Customer:
      type: object
      properties:
        nik:
          type: string
          example: 3374141414141414
        kk:
          type: string
          example: 3374010101010101
        fullname:
          type: string
          example: John Paul
        birthdate:
          type: string
          example: 01-01-1991
        mothers_maiden_name:
          type: string
          example: Paul Mother
        total_loan:
          type: integer
          example: 2
        total_loan_amount:
          type: integer
          example: 10000000
    Post_Loan:
      type: object
      properties:
        username:
          type: string
          example: jpaul
        loan_schema:
          type: string
          example: "Daily|Weekly|Monthly"
        total_loan:
          type: string
          example: 10000000
    Get_Loan:
      type: object
      properties:
        loans:
          type: array
          items: 
            $ref: '#/components/schemas/Loan'
    Loan:
      type: object
      properties:
        loan_id:
          type: string
          example: 3374141414141414
        total_loan:
          type: string
          example: 3374010101010101
        paid_loan:
          type: string
          example: John Paul
        next_payment_amount:
          type: string
          example: 01-01-1991
        loan_schema:
          type: string
          example: Paul Mother
    Post_Pay_Response:
      type: object
      properties:
        trx_id:
          type: string
          example: DANA-010102033333|VAC-3242343242342432423
        status:
          type: string
          example: pending|failed|success
    Get_Status_Response:
      type: object
      properties:
        status:
          type: string
          example: pending|failed|success
```
